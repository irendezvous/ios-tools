#!/bin/bash

echo "Delete current template folder"
rm -rf ~/Library/Developer/Xcode/Templates/FormsSyntronTemplates
echo "Make template folder"
mkdir ~/Library/Developer/Xcode/Templates/FormsSyntronTemplates
echo "Copy to template folder"
cp -a FormsSyntronTemplates/. ~/Library/Developer/Xcode/Templates/FormsSyntronTemplates