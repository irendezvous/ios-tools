To install do `./install.sh` or copy the templates manually to the path specified in the script. 

To use, simply press 'new file' in xcode and scroll down to "FormsSyntronTemplates" and pick a template. Then follow the instructions in the generated files.