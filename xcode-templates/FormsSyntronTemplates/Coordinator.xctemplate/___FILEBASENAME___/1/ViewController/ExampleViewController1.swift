// Copyright Forms Syntron Thailand

import CoreUI
import RxCocoa
import RxSwift

public class ExampleViewController1: UIViewController {

    // MARK: Properties
    
    private let disposeBag = DisposeBag()
    private let viewModel: ExampleViewModel1
    
    // MARK: Initialization
    
    init(viewModel: ExampleViewModel1) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupLayout()
        setupViewModelBindings()
    }
    
    // MARK: View
    
    private let layoutView = LayoutView()
    
    private let button: CUIFillButton = {
        let button = CUIFillButton()
        button.isEnabled = false
        return button
    }()
    
    // This is simply a container view to fill the LayoutView top area
    private let textFieldContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private let textField: CUIUnderlineTextField = {
        let textField = CUIUnderlineTextField(isDarkBackground: true)
        textField.text = ""
        textField.backgroundColor = .lightGray
        return textField
    }()
    
    private let informationLabel: UILabel = {
        let view = UILabel()
        view.text = "Write\n X = enable button\n Y = go to VC 1\n Z = go to VC 2 (final screen)"
        view.textAlignment = .left
        view.numberOfLines = 0
        return view
    }()
    
    private func setupLayout() {
        // Setup the main layout of the screen
        layoutView.addAndFillInViewSafeArea(view: view)
        layoutView.setViews(top: textFieldContainerView, 
                            bottom: button,
                            bottomPadding: 10.0)
        
        // Add the detailed views
        textFieldContainerView.addSubview(informationLabel)
        informationLabel.translatesAutoresizingMaskIntoConstraints = false
        textFieldContainerView.addConstraints([
            informationLabel.leadingAnchor.constraint(equalTo: textFieldContainerView.leadingAnchor, constant: 30),
            informationLabel.trailingAnchor.constraint(equalTo: textFieldContainerView.trailingAnchor, constant: -30),
            informationLabel.centerYAnchor.constraint(equalTo: textFieldContainerView.centerYAnchor),
            informationLabel.centerXAnchor.constraint(equalTo: textFieldContainerView.centerXAnchor)
        ])
        
        // Add the detailed views
        textFieldContainerView.addSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textFieldContainerView.addConstraints([
            textField.leadingAnchor.constraint(equalTo: textFieldContainerView.leadingAnchor, constant: 30),
            textField.trailingAnchor.constraint(equalTo: textFieldContainerView.trailingAnchor, constant: -30),
            textField.topAnchor.constraint(equalTo: informationLabel.bottomAnchor, constant: 30),
            textField.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    // MARK: View model bindings
    
    private func setupViewModelBindings() {
        viewModel.output.buttonEnabled
            .subscribe(onNext: { [weak self] state in 
                self?.button.isEnabled = state
            }).disposed(by: disposeBag)
        
        textField.rx.controlEvent(.editingChanged)
            .subscribe(onNext: { [weak self] _ in
                if let text = self?.textField.text {
                    self?.viewModel.input.viewControllerInput.onNext(text)
                }
            }).disposed(by: disposeBag)
    }
}
