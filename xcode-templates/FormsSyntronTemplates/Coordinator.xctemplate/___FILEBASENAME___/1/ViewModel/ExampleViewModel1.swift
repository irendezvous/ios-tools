// Copyright Forms Syntron Thailand

import RxSwift

public class ExampleViewModel1 {
    
    struct Input {
        // ViewController -> ViewModel
        //
        // These are inputs from the ViewController, e.g. a button was pressed
        // and the viewmodel needs to do some processing logic on the text
        // that is sent back from the ViewController.
        var viewControllerInput: AnyObserver<String>
        
        // Coordinator -> ViewModel
        //
        // These are inputs from the coordinator. Examples:
        // var someExample: AnyObserver<String>
    }
    
    struct Output {
        // ViewModel -> ViewController
        //
        // These are outputs for the ViewController to listen to, like
        // status updates to update the UI, or e.g. tablview data source
        // items
        var buttonEnabled: Observable<Bool>
        
        // ViewModel -> Coordinator
        //
        // These are outputs for the coordinator that presents the
        // ViewModel/ViewController to listen to, e.g. when the user 
        // have pressed some button, or the network requests have
        // finished and should kick off some kind of navigation
        var chosenPath: Observable<String>
    }
    
    let input: Input
    let output: Output
    
    private let disposeBag = DisposeBag()
    
    // Inputs
    private let viewControllerInputSubject = PublishSubject<String>()
    
    // Outputs
    private let buttonEnabledSubject = PublishSubject<Bool>()
    private let chosenPathSubject = PublishSubject<String>()
    
    // Constructor
    init() {
        input = Input(viewControllerInput: viewControllerInputSubject.asObserver())
        output = Output(buttonEnabled: buttonEnabledSubject.asObservable(), 
                        chosenPath: chosenPathSubject.asObservable())
        
        // Subscribe to inputs from ViewController -> ViewModel
        viewControllerInputSubject.subscribe(
            onNext: { [weak self] text in
                
                switch text {
                case "X":
                    // ViewModel -> ViewController
                    self?.buttonEnabledSubject.onNext(true)
                case "Y":
                    // ViewModel -> Coordinator
                    self?.chosenPathSubject.onNext(text)   
                case "Z": 
                    // ViewModel -> Coordinator
                    self?.chosenPathSubject.onNext(text)
                default:
                    // ViewModel -> ViewController
                    self?.buttonEnabledSubject.onNext(false)
                }
                
            }).disposed(by: disposeBag)
    }

}
