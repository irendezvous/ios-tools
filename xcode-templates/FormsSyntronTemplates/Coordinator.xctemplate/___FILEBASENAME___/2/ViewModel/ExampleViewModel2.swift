// Copyright Forms Syntron Thailand

import RxSwift

public class ExampleViewModel2 {
    
    struct Input {
        // ViewController -> ViewModel
         var someInput1: AnyObserver<String>
        
        // Coordinator -> ViewModel
         var someInput2: AnyObserver<String>
    }
    
    struct Output {
        // ViewModel -> ViewController
        var someOutput1: Observable<Bool>
        
        // ViewModel -> Coordinator
        var someOutput2: Observable<String>
    }
    
    let input: Input
    let output: Output
    
    private let disposeBag = DisposeBag()
    
    // Inputs
    private let someInput1Subject = PublishSubject<String>()
    private let someInput2Subject = PublishSubject<String>()
    
    // Outputs
    private let someOutput1Subject = PublishSubject<Bool>()
    private let someOutput2Subject = PublishSubject<String>()
    
    // Constructor
    init() {
        input = Input(someInput1: someInput1Subject.asObserver(),
                      someInput2: someInput2Subject.asObserver())
        output = Output(someOutput1: someOutput1Subject.asObservable(), 
                        someOutput2: someOutput2Subject.asObservable())
        
        // Subscribe ViewController -> ViewModel
        someInput1Subject.subscribe(
            onNext: { [weak self] text in
                // someOutput1.onNext .............
            }).disposed(by: disposeBag)
        
        // Subscribe ViewController -> ViewModel
        someInput2Subject.subscribe(
            onNext: { [weak self] text in
                // someOutput2.onNext .............
            }).disposed(by: disposeBag)
    }

}
