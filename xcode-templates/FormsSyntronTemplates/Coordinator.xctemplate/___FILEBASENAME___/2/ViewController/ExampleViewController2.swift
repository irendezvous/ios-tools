// Copyright Forms Syntron Thailand

import CoreUI
import RxCocoa
import RxSwift

public class ExampleViewController2: UIViewController {

    // MARK: Properties
    
    private let disposeBag = DisposeBag()
    private let viewModel: ExampleViewModel2
    
    // MARK: Initialization
    
    init(viewModel: ExampleViewModel2) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupLayout()
        setupViewModelBindings()
    }
    
    // MARK: View
    
    private let layoutView = LayoutView()
    
    private let label1: UILabel = {
        let view = UILabel()
        view.text = "Nothing to see here.. To finish the coordinator flow to go the other VC"
        view.textAlignment = .center
        view.layer.borderWidth = 2
        view.numberOfLines = 0
        return view
    }()
        
    private let label2: UILabel = {
        let view = UILabel()
        view.text = "Hello"
        view.textAlignment = .center
        view.layer.borderWidth = 2
        return view
    }()
    
    private func setupLayout() {
        // Setup the main layout of the screen
        layoutView.addAndFillInViewSafeArea(view: view)
        layoutView.setViews(top: label1, 
                            bottom: label2)
    }
    
    // MARK: View model bindings
    
    private func setupViewModelBindings() {
        // Do binding
    }
}
