// Copyright Forms Syntron Thailand

import CoreUI
import RxCocoa
import RxSwift

public class ExampleViewController3: UIViewController {

    // MARK: Properties
    
    private let disposeBag = DisposeBag()
    private let viewModel: ExampleViewModel3
    
    // MARK: Initialization
    
    init(viewModel: ExampleViewModel3) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupLayout()
        setupViewModelBindings()
    }
    
    // MARK: View
    
    private let layoutView = LayoutView()
    
    private let label1: UILabel = {
        let view = UILabel()
        view.text = "Read console output"
        view.textAlignment = .center
        view.layer.borderWidth = 2
        return view
    }()
        
    private let button: CUIFillButton = {
        let view = CUIFillButton()
        view.setTitle("finish", for: .normal)
        return view
    }()
    
    private func setupLayout() {
        // Setup the main layout of the screen
        layoutView.addAndFillInViewSafeArea(view: view)
        layoutView.setViews(top: label1, 
                            bottom: button,
                            bottomHeight: 300.0,
                            bottomPadding: 20.0)
    }
    
    // MARK: View model bindings
    
    private func setupViewModelBindings() {
        button.rx.controlEvent(.touchUpInside)
            .subscribe { [weak self] _ in
                self?.viewModel.input.buttonPressed.onNext(())
            }.disposed(by: disposeBag)
    }
}
