// Copyright Forms Syntron Thailand

import RxSwift

public class ExampleViewModel3 {
    
    struct Input {
        // ViewController -> ViewModel
         var buttonPressed: AnyObserver<Void>
        
        // Coordinator -> ViewModel
        // Nothing
    }
    
    struct Output {
        // ViewModel -> ViewController
        // Nothing
        
        // ViewModel -> Coordinator
        var userAgreed: Observable<Void>
    }
    
    let input: Input
    let output: Output
    
    private let disposeBag = DisposeBag()
    
    // Inputs
    private let buttonPressedSubject = PublishSubject<Void>()
    
    // Outputs
    private let userAgreedSubject = PublishSubject<Void>()
    
    // Constructor
    init() {
        input = Input(buttonPressed: buttonPressedSubject.asObserver())
        output = Output(userAgreed: userAgreedSubject.asObservable())
        
        // Subscribe ViewController -> ViewModel
        buttonPressedSubject.subscribe { [weak self] _ in
            self?.userAgreedSubject.onNext(())
        }.disposed(by: disposeBag)
    }

}
