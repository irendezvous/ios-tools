// Copyright Forms Syntron Thailand

import Core
import Foundation
import RxSwift

Steps to make this file add to project properly

 - Remove the folder REFERENCES from the project
 - Move the actual folders/files to the proper location in the repo (by default it adds in root..)
 - Drag the files into the project again and chose groups, not folder references

Proper template might solve this but seems difficult?

public class ___FILEBASENAME___: Coordinator {
        
    // Public interface, meant for this coordinators parent
    // (also a coordinator) to subscribe to
    public let outputStatus = PublishSubject<Bool>()

    // Properties
    private let disposeBag = DisposeBag()
    
    // Sub viewcontrollers this coordinator can present
    var exampleViewController1: ExampleViewController1?
    var exampleViewController2: ExampleViewController2?
    var exampleViewController3: ExampleViewController3?
    
    override public func start() {
        let exampleViewModel1 = ExampleViewModel1()
        exampleViewController1 = ExampleViewController1(viewModel: exampleViewModel1)
        navigationController.pushViewController(exampleViewController1!, animated: true)
        
        exampleViewModel1.output.chosenPath
            .subscribe { [weak self] path in
                switch path.element {
                case "Y":
                    self?.startPath2Flow()
                case "Z":
                    self?.startPath3Flow()
                default:
                    print("Unknown path..")
                }
            }.disposed(by: disposeBag)
    }
    
    // MARK: Flows

    private func startPath2Flow() {
        let exampleViewModel2 = ExampleViewModel2()
        exampleViewController2 = ExampleViewController2(viewModel: exampleViewModel2)
        navigationController.pushViewController(exampleViewController2!, animated: true)
    }
    
    private func startPath3Flow() {
        let exampleViewModel3 = ExampleViewModel3()
        exampleViewController3 = ExampleViewController3(viewModel: exampleViewModel3)
        navigationController.pushViewController(exampleViewController3!, animated: true)
        
        // This will 
        exampleViewModel3.output.userAgreed
            .subscribe { [weak self] _ in
                print("Flow 3 has finished, so now we send that the coordinat finished too")
                self?.outputStatus.onNext(true)
            }.disposed(by: disposeBag)
    }
}
