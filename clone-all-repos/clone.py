# coding: utf8

import requests
import json

# 
# Shared
# 

headers = { 'Content-Type': 'application/json' }
auth = ('username here', 'password here')

# 
# Get the repos
# 

params = (('q', 'project.key=\"KMM\"'), ("pagelen", "100"))
response = requests.get('https://api.bitbucket.org/2.0/repositories/fsdi', headers=headers, params=params, auth=auth)
repos = response.json()

# print repos

for repo in repos["values"]:
	print "git clone %s; " % repo['links']['clone'][1]['href']

# So that when you copy all lines into shell, the last line is also
# automatically run. 
print "echo; "