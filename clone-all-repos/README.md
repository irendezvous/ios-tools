This will generate shell commands to clone every repository in `https://bitbucket.org/fsdi/workspace/projects/KMM`. Simply add your BB authentication details and run the script.

Then copy the output into shell and run it in the folder you want want all the repos to be placed. 

This will not overwrite already-cloned repos, so you can run it over and over to get all new repos.