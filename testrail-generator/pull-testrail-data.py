# coding: utf8

from datetime import datetime
import requests
import json
import re

def save(name, text, path, extension):
	filename = "%s/%s.%s" % (path, name, extension)
	print "Generating file %s" % filename
	with open(filename, 'w') as fout:
		fout.write(text.encode('utf8'))

# 
# Shared
# 

headers = { 'Content-Type': 'application/json' }
auth = ('jonathan.l@forms-syntron.co.th', 'G-t9wXMVNpDwr3DF')

# 
# Get the sections / categories
# 

params = (('/api/v2/get_sections/8', ''),)
response = requests.get('https://fsth.testrail.io/index.php', headers=headers, params=params, auth=auth)
sections = response.json()
directory = "/Users/jonathanwingerlang/Work/ios-tools/testrail-generator/out-json"
save("sections", response.text, directory, "json")

# 
# Get the test cases recursively since max is 250..
# 
def getTestCasesAndStoreFileForURL(uurrll):
	headers = { 'Content-Type': 'application/json', 'x-api-ident': 'beta' }
	response = requests.get('https://fsth.testrail.io/index.php?'+uurrll, headers=headers, auth=auth)
	apiResponse = response.json()

	# 
	# Store for later
	# 
	fileName = "data_%s_to_%s" % (apiResponse["offset"], apiResponse["offset"] + apiResponse["size"])
	directory = "/Users/jonathanwingerlang/Work/ios-tools/testrail-generator/out-json"
	save(fileName, response.text, directory, "json")
	cases = apiResponse["cases"]

	if apiResponse['_links'] != None:
		if apiResponse['_links']['next'] != None:
			print apiResponse['_links']['next']
			getTestCasesAndStoreFileForURL(apiResponse['_links']['next'])

# Kickoff
getTestCasesAndStoreFileForURL('/api/v2/get_cases/8')

print "done"