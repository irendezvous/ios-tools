import json
import glob 

testNamePrefixes = []
testCaseToFile = {}

files = glob.glob('out-json/data*.json')
for single_file in files:
	with open(single_file, "r") as file:
		data = json.load(file)["cases"]
		for case in data:
			testNamePrefix = "test_%s" % case['id']
			testNamePrefixes.append(testNamePrefix)
			testCaseToFile[testNamePrefix] = None


import os
rootdir=('/Users/jonathanwingerlang/Work/KTC/src')
for folder, dirs, files in os.walk(rootdir):
	for file in files:
		if file.endswith('.generated.swift'):
			fullpath = os.path.join(folder, file)
			with open(fullpath, 'r') as f:
				for line in f:
					for testNamePrefix in testNamePrefixes:
						if testNamePrefix in line:
							testCaseToFile[testNamePrefix] = fullpath
							break

for key in testCaseToFile.keys():
	if testCaseToFile[key] == None:
		print "%s is not in any project" % key

# for key in testCaseToFile.keys():
# 	if testCaseToFile[key] != None:
# 		print "%s is found in a project" % key