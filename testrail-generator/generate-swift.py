import glob
import json
from datetime import datetime
import re

# 
# Shared
# 

space = re.compile('\ ')
anything = re.compile('[^a-zA-Z0-9]')

def save(name, text, path, extension):
	filename = "%s/%s.%s" % (path, name, extension)
	print "Generating file %s" % filename
	with open(filename, 'w') as fout:
		fout.write(text.encode('utf8'))

def getSectionName(inid,sections):
	items = list(filter(lambda s: s['id'] == inid, sections))
	if len(items) == 0:
		return ""
	else:
		return items[0]['name']

# 
# Load sections
# 
with open("/Users/jonathanwingerlang/Work/ios-tools/testrail-generator/out-json/sections.json", 'r') as f:
	sections = json.load(f)

# 
# Gather source files
# 
all_cases = []
files = glob.glob('out-json/data*.json')
for single_file in files:
	print "========"
	print "======== Generating file %s" % single_file
	print "========"
	with open(single_file, 'r') as f:
		x = json.load(f)["cases"]
		all_cases.extend(x)
print all_cases
# 
# Generate code
# 

for section in sections:
	print "========"
	print "======== Generating section %s" % section["id"]
	print "========"

	file = ""
	file += "// This file is auto generated with test cases from TestRail"
	file += "\n// Generated on: %s" % datetime.now().strftime("%m-%d-%Y, %H:%M:%S")
	file += "\n"

	parentSectionName = getSectionName(section['parent_id'], sections)
	sectionName = getSectionName(section['id'], sections)
	
	parentSectionName = parentSectionName.title()
	parentSectionName = parentSectionName.replace('\n', '')
	sectionName = sectionName.title()
	sectionName = sectionName.replace('\n', '')
	
	protocolName = ""
	if parentSectionName != "":
		protocolName = "%sXXXXXX%s" % (parentSectionName, sectionName)
	else:
		protocolName = "%s" % sectionName
	
	protocolName = protocolName.replace('\n', '')
	protocolName = space.sub('_', protocolName)
	protocolName = anything.sub('', protocolName)
	protocolName = protocolName.replace('XXXXXX', '_')
	protocolName = "%s_UITests" % protocolName
	file += "\n// swiftlint:disable file_length"
	file += "\n"
	file += "\nimport XCTest"
	file += "\n"
	file += "\nprotocol %s: XCTestCase {" % protocolName

	for case in all_cases:
		if case['section_id'] == section['id']:
			print "============ Generating case %s (section id: %s)" % (case["id"], case["section_id"])
			title = case['title']

			# Clean the title from special chars
			title = space.sub('_', title)
			title = anything.sub('_', title)
			title = title.replace('_____', '_')
			title = title.replace('____', '_')
			title = title.replace('___', '_')
			title = title.replace('__', '_')
			title = title.rstrip('_')

			sectionTitle = ""
			if parentSectionName != "":
				sectionTitle = "%s - %s" % (parentSectionName, sectionName)
			else:
				sectionTitle = "%s" % sectionName

			file += "\n    ///"
			file += "\n    /// Original title:"
			file += "\n    /// %s" % case['title'].replace('\n', '')
			file += "\n    ///"
			file += "\n    /// Testrail ID: %s" % case['id']
			file += "\n    /// Section: %s" % sectionTitle
			file += "\n    /// Updated: %s" % case['updated_on']
			file += "\n    ///"
			file += "\n    /// Preconditions:"
			if case['custom_preconds'] != None:
				preconds = case['custom_preconds'].replace('\n', '\n    /// ')
				file += "\n    /// " + preconds
			else:
				file += "\n    /// None"
			file += "\n    ///"
			file += "\n    /// Expectations:"
			expected = case['custom_expected'].replace('\n', '\n    /// ')
			file += "\n    /// " + expected
			file += "\n    ///"
			file += "\n    /// Steps:"
			steps = case['custom_steps'].replace('\n', '\n    /// ')
			file += "\n    /// " + steps
			file += "\n    ///"
			file += "\n    func test_%s_%s() throws" % (case['id'], title)
			file += "\n"
	file += "\n}"

	directory = "/Users/jonathanwingerlang/Work/ios-tools/testrail-generator/out-protocols"
	save(protocolName, file, directory, "generated.swift")

	# 
	# For easy import but not to be used more than that
	# 
	implFile = """import XCTest

//swiftlint:disable type_name

class %s_implementation: XCTestCase, %s { }""" % (protocolName, protocolName)
	directory = "/Users/jonathanwingerlang/Work/ios-tools/testrail-generator/out-implementations"
	save(protocolName, implFile, directory, "swift")