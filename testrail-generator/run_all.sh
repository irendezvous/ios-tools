#!/bin/bash

echo "Clear local data"
rm -rf out-implementations/*.swift
rm -rf out-protocols/*.swift
rm -rf out-json/*.json

echo "Load sections and cases json"
python pull-testrail-data.py

echo "Generate swift files"
python generate-swift.py

echo "Inspect local project for missing test implementations"
python check-local-project.py