import os
from distutils.dir_util import copy_tree
import shutil

# 
# Helper
# 

def inplace_change(filename, old_string, new_string):
    with open(filename) as f:
        s = f.read()
        if old_string not in s:
            print('"{old_string}" not found in {filename}.'.format(**locals()))
            return
    with open(filename, 'w') as f:
        print('Changing "{old_string}" to "{new_string}" in {filename}'.format(**locals()))
        s = s.replace(old_string, new_string)
        f.write(s)

# 
# Start
# 

print "Clean dir"
shutil.rmtree('tmp/')

print "Copying template to temp folder"
copy_tree("module-template", "tmp")

module_name = raw_input("What is name of module: ")

print "Replace placeholders in files"
inplace_change("tmp/Example/project.yml", "XX_MODULE_NAME_XX", module_name)
inplace_change("tmp/XX_MODULE_NAME_XX.podspec", "XX_MODULE_NAME_XX", module_name)
inplace_change("tmp/Example/Podfile", "XX_MODULE_NAME_XX", module_name)

print "Rename files"

podspec_file = "tmp/%s.podspec" % module_name
os.rename("tmp/XX_MODULE_NAME_XX.podspec", podspec_file)

example_app_code_dir = "tmp/Example/%s" % module_name
os.rename("tmp/Example/XX_MODULE_NAME_XX", example_app_code_dir)

pod_code_dir = "tmp/%s" % module_name
os.rename("tmp/XX_MODULE_NAME_XX", pod_code_dir)

print "Generating xcproject file for example"
os.system("cd tmp/Example; xcodegen;")

print "Remove project.yml"
os.remove('tmp/Example/project.yml')

print "Run pod install in Example"
os.system("cd tmp/Example; pod install;")

print "Done"




