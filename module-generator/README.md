
This will help to generate a new module, including podspec, CoreTesting examples already setup etc.

Use with `python module.py` then enter a name of the module.

Requires `brew install xcodegen` to function.
