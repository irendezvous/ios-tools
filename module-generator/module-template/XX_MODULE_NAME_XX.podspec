Pod::Spec.new do |s|
  s.name             = 'XX_MODULE_NAME_XX'
  s.version          = '0.1.0'
  s.summary          = 'A short description of XX_MODULE_NAME_XX.'
  s.description      = 'XX_MODULE_NAME_XX module'
  s.homepage         = 'https://bitbucket.org/fsdi/workspace/projects/KMM'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'someone@forms.com' => 'someone@forms.com' }
  s.source           = { :git => 'git@bitbucket.org:fsdi/XX_MODULE_NAME_XX.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.swift_version    = '5.0'
  s.source_files     = 'XX_MODULE_NAME_XX/**/*'
    
  s.dependency 'APILayer'
  s.dependency 'Core'
  s.dependency 'KTCCore'
  s.dependency 'RxSwift'
end
