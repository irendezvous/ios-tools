// Copyright Forms Syntron Thailand

import CoreTesting
import RxSwift
import CoreUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let disposeBag = DisposeBag()
    var scenarioCoordinator: ScenarioCoordinator!

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if ProcessInfo.processInfo.arguments.contains("UITEST") {
            UIView.setAnimationsEnabled(false)
        }
        
        KTCMobileFont.registerFonts
        
        let navController = UINavigationController()

        scenarioCoordinator = ScenarioCoordinator(navigationController: navController)
        let scenarios: [Scenario] = [
            Scenario1(),
            Scenario2()
        ]
        let viewModel = ScenarioListViewModel(scenarios: scenarios)
        scenarioCoordinator.start(viewModel: viewModel)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()        
        return true
    }
}
