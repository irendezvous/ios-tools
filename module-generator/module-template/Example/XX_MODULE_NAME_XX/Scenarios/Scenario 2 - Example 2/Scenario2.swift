// Copyright Forms Syntron Thailand

import Core
import CoreTesting
import Foundation
import Networking
import APILayer
import Localize

struct Scenario2: Scenario {
    
    var coordinatorType: Coordinator.Type = ExampleCoordinator.self
    
    var name: String = "Scenario 2 - Example 2"
    
    var steps: String = "This displays a specific viewcontroller"
    
    var language = LocalizeType.EN
    
    var fakedEndpoints: [FakeEndpoint] = [
        // Example
        // FakeEndpoint(path: SomeService.endpointPath, file: "SomeService_scenario2.json")
    ]
    
    func setupCoordinator(_ coordinator: Coordinator) {
        // We display a specific view controller in this scenario
    }
    
    func specificViewControllerToNavigateTo() -> UIViewController? { 
        return ExampleViewController()
    }
}
