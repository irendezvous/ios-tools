// Copyright Forms Syntron Thailand

import Core
import CoreTesting
import Foundation
import Networking
import APILayer
import Localize

class ExampleCoordinator: Coordinator {
    
    open override func start() {
        let vc = ExampleViewController()
        navigationController.setViewControllers([vc], animated: true)
    }
}

class ExampleViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let label = UILabel()
        label.text = "ExampleViewController"
        label.textAlignment = .center
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints([
            label.heightAnchor.constraint(equalToConstant: 100),
            label.widthAnchor.constraint(equalToConstant: 300),
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}

struct Scenario1: Scenario {
    
    var coordinatorType: Coordinator.Type = ExampleCoordinator.self
    
    var name: String = "Scenario 1 - Example 1"
    
    var steps: String = "This starts a coordinator"
    
    var language = LocalizeType.EN
    
    var fakedEndpoints: [FakeEndpoint] = [
        // Example
        // FakeEndpoint(path: SomeService.endpointPath, file: "SomeService_scenario2.json")
    ]
    
    func setupCoordinator(_ coordinator: Coordinator) {
        // We display a specific view controller in this scenario
    }
    
    func specificViewControllerToNavigateTo() -> UIViewController? { 
        return nil 
    }
}
